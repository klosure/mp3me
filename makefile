PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
install:
	cp ./mp3me.sh $(BINDIR)/mp3me
	cp ./mp3me.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/mp3me
	rm $(MANDIR)/mp3me.1
test:
	shellcheck mp3me.sh
