# mp3me

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)

### Motivation

*Simple and fast mp3 conversion*

I was using the [soundconverter](https://soundconverter.org/) tool on my main workstation. I would still recommend it, but I found myself using more command line focused applications. I wanted to see if I could write something that would satisfy my needs. I've tried to make this tool as intuitive as possible, with sane defaults, and no weird flags. 

### Dependencies
In order to use this tool, you will need a few things installed:

- [FFmpeg](https://ffmpeg.org)
- [GNU Parallel](https://www.gnu.org/software/parallel)
- POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)
- POSIX [shell](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)

### Installation
```bash
make install      #(as root)

# to uninstall
make uninstall    #(as root)
```

### Usage
```bash
mp3me ./musicdir                   # convert music files
mp3me -j 8 ./musicdir              # start 8 jobs, i.e. threads (default is half your available cores)
mp3me -b 192 ./musicdir            # convert music files @ 192kbps
mp3me -h                           # show this help message
mp3me -v                           # show the version info
```

### Limitations
To make this tool simpler, I have made a few assumptions. We only convert to mp3. Files are output in the same folder. These are my preferred settings. Maybe I'll add more flags to handle changing output type and location.

### License / Disclaimer
This project is licensed under the 3-clause BSD license. (See LICENSE.md)
</br>
No WAVs were harmed in the making of this tool.

